# <tt>issuebot</tt>

Our friendly robot that reads issues and merge requests, and responds
inline. It is designed around GitLab CI as the service that runs it,
and integrates tightly with it.


# Modules

This is built around modules that can run separately, crash,
etc. without affecting the whole process. Each module also returns
its results as a YAML dictionary to _stdout_ so that the core
_issuebot_ can format all the results into a nice report which is
posted in a single message.


## Variables

Things that need to be shared across multiple modules should be
exported as environment variables. GitLab CI handles environment
variables quite well. The _ApplicationID_ is also used to name the
APK and the local source folder, following F-Droid conventions.


* _ISSUEBOT_CURRENT_APPLICATION_ID_ - the _ApplicationID_ of the current app
* _ISSUEBOT_CURRENT_ISSUE_ID_ - the number of the GitLab Issue currently being processed
* _ISSUEBOT_CURRENT_SOURCE_URL_ - the URL to the source repo for the current app, if this is set, then a local copy of the source code will be available at _build/\<ApplicationID\>_
* _ISSUEBOT_CURRENT_REPLY_FILE_ - the full path to where the JSON output report should be written


## Testing

To test individual modules on your project:

* put the app source into a _build/\<appid\>/_ directoy
* put the build apk into a _repo/_ directory
* run `ISSUEBOT_CURRENT_APPLICATION_ID=<appid> modules/<module>.py`

The output will be below the _public/_ directory.


## Code Format

This project uses Black to automatically format all the Python code.  It uses
the version of Black that is in Debian/stable.  To format the code, run:

```bash
black --skip-string-normalization issuebot issuebot.py test_issuebot.py
```


## API

There are lots of helper functions and methods for common operations
available to _issuebot_ modules that subclass `IssuebotModule`.


## Module Setup

_issuebot_ will install and remove things for the modules. It will
currently handle _apt-get_ and _pip_. Add a space separated list of
packages to these variable names in a comment:

* _issuebot_apt_install_ - list of packages to install using `apt-get install`
* _issuebot_apt_remove_ - list of packages to uninstall using `apt-get purge`
* _issuebot_pip_install_ - list of packages to install with `pip install`
* _issuebot_pip_remove_ - list of packages to install with `pip uninstall`


## Return Results in the Reply File

Each module "replies" to _issuebot_ by writing out to a file. This file must be
written out to the full file path given in _ISSUEBOT_CURRENT_REPLY_FILE_. The
file data format is either YAML or JSON, with this format:

```yaml
# A string with Markdown posted as part of the comment in the issue
report: |
  <details>
  <summary>This stuff was found</summary>
  <ul>
  <li>scary</li>
  <li>beautiful</li>
  <li>awful</li>
  <li>ugly</li>
  </ul>
  </details>
# any data you want, in a dict form
reportData: {}
# any emoji to add to the issue (currently flaky)
emoji:
  - tada
# any labels to add to the issue
labels:
  - on-fdroid.org
```

This reply can also be JSON:

```json
{
  "applicationId": "",
  "emoji": [],
  "labels": [],
  "report": "",
  "reportData": {}
}
```

* `applicationId`: package name of the app (e.g. `'org.fdroid.fdroid'`)
* `emoji`: any emoji to add to the issue (currently flaky), e.g. `['robot','warning','radioactive']`
* `labels`: any labels to add to the issue, e.g. `['antifeatures', 'scanner-error']`
* `report`: HTML/Markdown posted as part of the comment in the issue, including heading (`<h3>`) for the report. Make sure you close all HTML tags you open here!
* `reportData`: any data you want in a dict form. Inside can be dicts and/or arrays; this is then offered up as part of the API for people to consume


## `fdroid-bot` label

[_issuebot_](https://gitlab.com/fdroid/issuebot) runs on a schedule to
scan all new submissions and post its reports directly to the issue or
merge request. Once _issuebot_ completes posting its report, it will
add the `fdroid-bot` label to the issue or merge request. That tells
_issuebot_ to ignore the issue in future runs. If you want _issuebot_
to run on any item again, remove the `fdroid-bot` label. On the next
run, _issuebot_ will consider that issue as if it had not seen it.


# JSON REST API

_issuebot_ also provides a JSON API on top of the current HTML
reports, so that any web app can use the data in their own user
experience. This can be combined with our other developing APIs, like
the [_buildserver_ status API](https://f-droid.org/repo/status/running.json).
There are two entry points:

* source code URL: [RFP](https://fdroid.gitlab.io/rfp/issuebot/sourceUrls.json) / [_fdroiddata_](https://fdroid.gitlab.io/fdroiddata/issuebot/sourceUrls.json)
* Application ID: [RFP](https://fdroid.gitlab.io/rfp/issuebot/applicationIds.json) / [_fdroiddata_](https://fdroid.gitlab.io/fdroiddata/issuebot/applicationIds.json)

Here is an excerpt of the Application ID entry point that shows the
_issuebot_ runs for one specific app. There are two runs, with the
most recent first. The newer version has the results from more
modules that were added since the first run.

```json
  "at.roteskreuz.stopcorona": [
    {
      "issueId": 1319,
      "jobId": "925261691",
      "modules": [
        "925261691/1319/active-hostnames.py.json",
        "925261691/1319/check-for-translation-service.py.json",
        "925261691/1319/dependencies-scrape.py.json",
        "925261691/1319/fastlane.py.json",
        "925261691/1319/fdroid-scan-apk.py.json",
        "925261691/1319/fdroid-scanner.py.json",
        "925261691/1319/gradle-productFlavors.py.json",
        "925261691/1319/gradle-wrapper.py.json",
        "925261691/1319/links-to-services.py.json",
        "925261691/1319/sdk.py.json",
        "925261691/1319/suspicious-names.py.json",
      ],
      "successfulBuilds": []
    },
    {
      "issueId": 1319,
      "jobId": "600681223",
      "modules": [
        "600681223/1319/check-for-translation-service.py.json",
        "600681223/1319/dependencies-scrape.py.json",
        "600681223/1319/fastlane.py.json",
        "600681223/1319/fdroid-scan-apk.py.json",
        "600681223/1319/fdroid-scanner.py.json",
        "600681223/1319/gradle-productFlavors.py.json",
        "600681223/1319/gradle-wrapper.py.json"
        "600681223/1319/sdk.py.json",
        "600681223/1319/suspicious-names.py.json",
      ]
    }
  ],
```

The ultimate entry is a catalog of what _issuebot_ saw in one
particular app in one particular run, identified by the GitLab CI
Job ID. That includes a list of relative paths to the JSON output of
the _issuebot_ modules that ran. The easiest way to get started with
the API right now is to download the whole collection of files from
the GitLab CI Job: go to the RFP
[Pipelines](https://gitlab.com/fdroid/rfp/-/pipelines) and click the
most recent download link on the right-most column.


# Installing into a GitLab Project

1. Create a "bot" account GitLab.
2. Get "bot" account's "Personal Access Token" with "api" and "read_repository" scope.
3. Add the Personal Access Token to the GitLab project's CI/CI Variables using the name `PERSONAL_ACCESS_TOKEN` (it should be set to "Protected" and "Masked").
3. Optionally create a "bot" account on GitHub with the same name, give it all read scopes, and set the API token in the GitLab project's CI/CI Variables using the name `GITHUB_TOKEN` (it should be set to "Protected" and "Masked").
4. Optionally add any other API access tokens there that might be needed, e.g. `EXODUS_PRIVACY_TOKEN`, `VIRUSTOTAL_API_KEY`, etc.
6. If this instance allows working without source code, you might want to set `ISSUEBOT_PROCESS_WITHOUT_SOURCE`
7. In the "rfp" project settings, e.g. `https://gitlab.com/fdroid/rfp/-/settings/ci_cd#js-pipeline-triggers`, add a [webhook trigger URL](https://docs.gitlab.com/ee/ci/triggers/#use-a-webhook) then add it to the hooks settings, e.g. `https://gitlab.com/fdroid/rfp/-/hooks`.
![](issuebot-gitlab-webhook.png)
