#!/usr/bin/env python3

import hashlib
import inspect
import json
import os
import re
import sys

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), "..")
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot
from issuebot import IssuebotModule


class GradleWrapper(IssuebotModule):
    def __init__(self):
        super().__init__()
        f = 'checksums.json'
        download = True
        if os.path.exists(f):
            with open(f) as fp:
                try:
                    self.checksums = json.load(fp)
                    download = False
                except Exception:
                    os.remove(f)
        while download:
            url = 'https://gitlab.com/fdroid/gradle-transparency-log/-/raw/master/checksums.json'
            print('Fetching', url)
            r = issuebot.requests_get(url)
            if r.status_code != 200:
                print(r.status_code, '- retrying')
            else:
                self.checksums = r.json()
                with open(f, 'w') as fp:
                    json.dump(self.checksums, fp)
                break
        self.sha256s = dict()
        self.urls = dict()
        for url, sha256s in self.checksums.items():
            for sha256_dict in sha256s:
                sha256 = sha256_dict['sha256']
                if sha256 not in self.urls:
                    self.urls[sha256] = []
                self.urls[sha256].append(url)
                if url not in self.sha256s:
                    self.sha256s[url] = []
                self.sha256s[url].append(sha256)

    def check_properties(self, root, f):
        path = os.path.join(root, f)
        display_path = os.path.relpath(path, self.source_dir)
        propdata, properties = issuebot.read_properties(path)
        distributionUrl = properties['distributionUrl'].replace('\\:', ':')
        if not distributionUrl.startswith('https:'):
            self.add_label('insecure-gradlew')
            self.report = (
                '<details open><summary><b>ERROR:</b></summary>'
                + '<em>gradle/wrapper/gradle-wrapper.properties</em> must use HTTPS in <tt>distributionUrl</tt>!'
                + '</details>'
                + self.report
            )
            distributionUrl = distributionUrl.replace('http:', 'https:')
        if not distributionUrl.startswith(
            'https://services.gradle.org/distributions/gradle-'
        ):
            self.add_label('insecure-gradlew')
            self.report = (
                '<details><summary>non-standard source</summary>'
                + '<em>gradle/wrapper/gradle-wrapper.properties</em> uses non-standard source '
                + ('for downloading gradle: %s</details>\n' % distributionUrl)
                + self.report
            )

        sha256s = self.sha256s.get(distributionUrl, [])
        distributionSha256Sum = properties.get('distributionSha256Sum')
        if distributionSha256Sum and sha256s and distributionSha256Sum not in sha256s:
            self.add_label('insecure-gradlew')
            self.report += (
                '* `distributionSha256Sum=%s` should be `distributionSha256Sum=%s`!\n'
                % (distributionSha256Sum, sha256s[0])
            )
        if not distributionSha256Sum and sha256s:
            self.add_label('insecure-gradlew')
            self.report += (
                '<details><summary>missing <tt>distributionSha256Sum</tt></summary><p>'
                + '<em>gradle/wrapper/gradle-wrapper.properties</em> is missing [distributionSha256Sum]('
                + 'https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:verification).\n\n'
                + 'This means that the gradle download is not verified.\n\n'
                + 'We recommend explicitly setting the expected Sha256sum to protect you and your apps '
                + 'if a bad actor gets access to the Gradle servers or manages to MitM your internet connection.\n\n'
                + 'Please note that Android Studio does not currently support this and may or may '
                + ' not crash, but we suggest you try our suggestion anyway to improve the '
                + 'security of yourself and your app.\n\n\n\n'
                + 'Here is an example of how to fix this:\n\n'
                + '```properties\n{propdata}\ndistributionSha256Sum={sha256}\n```\n\n</p></details>'.format(
                    propdata=propdata.strip(), sha256=sha256s[0]
                )
            )
        self.reportData[display_path] = {
            'distributionUrl': distributionUrl,
            'sha256SumOfDistributionUrl': sha256s,
            'distributionSha256Sum': distributionSha256Sum,
        }

    def check_jar(self, root, f):
        gradle_jar = os.path.join(root, f)
        hasher = hashlib.sha256()
        with open(gradle_jar, 'rb') as fp:
            hasher.update(fp.read())
        sha256 = hasher.hexdigest()
        properties_path = gradle_jar[:-4] + '.properties'
        propdata, properties = issuebot.read_properties(properties_path)
        distributionUrl = properties.get('distributionUrl', '').replace('\\://', '://')
        declared_url = re.sub(r'(bin|all).zip', r'wrapper.jar', distributionUrl)
        urls = self.urls.get(sha256)
        if not urls:
            local_url = None
        elif declared_url in urls:
            local_url = declared_url
        else:
            local_url = urls[-1]
        display_properties = os.path.relpath(properties_path, self.source_dir)
        display_jar = os.path.relpath(gradle_jar, self.source_dir)
        properties_link = self.get_source_url(display_properties)
        jar_link = self.get_source_url(display_jar)
        self.reportData[display_jar] = {
            'sha256': sha256,
            'localUrl': local_url,
            'properties': display_properties,
            'declaredUrl': declared_url,
            'distributionUrl': distributionUrl,
        }

        sha256s = self.sha256s.get(distributionUrl, [])
        if len(sha256s) == 0:
            correct_sha256 = (
                '1f3067073041bc44554d0efe5d402a33bc3d3c93cc39ab684f308586d732a80d'
            )
        else:
            correct_sha256 = sha256s[0]

        upgrading_howto = (
            '<p>There is a gradle command for [upgrading](%s) the wrapper:\n\n'
            '```bash\n./gradlew wrapper --gradle-version %s \\\n'
            '  --gradle-distribution-sha256-sum %s'
            '\n```\n\n</p>'
        ) % (
            'https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:upgrading_wrapper',
            os.path.basename(declared_url.replace('-wrapper.jar', '')).split('-')[-1]
            or '5.6.4',
            correct_sha256,
        )
        if not local_url:
            self.add_label('insecure-gradlew')
            self.add_label('non-free')
            self.report = (
                '<details open><summary><b>ERROR:</b></summary>\n'
                + '[_%s_](%s) cannot be verified as free software because it'
                + ' does not match any listed on the [official website]'
                + '(https://gradle.org/release-checksums/): <tt>%s</tt>'
                + upgrading_howto
                + '</details>'
            ) % (display_jar, jar_link, sha256) + self.report
        elif local_url != declared_url:
            self.add_label('insecure-gradlew')
            self.report += (
                '<details><summary>gradle vs wrapper version mismatch</summary>'
                + '<em>%s</em> is [%s](%s.sha256), but [_%s_](%s) declares <tt>%s</tt> as the gradle version.'
                + upgrading_howto
                + '</details>'
            ) % (
                display_jar,
                os.path.basename(local_url),
                local_url,
                display_properties,
                properties_link,
                distributionUrl,
            )

    def main(self):
        if not os.path.isdir(self.source_dir):
            print(self.source_dir, 'does not exist, skipping')
            return
        issuebot.run_cli_tool(
            ['git', '-C', os.path.relpath(self.source_dir), 'reset', '--hard']
        )
        self.report = ''
        self.reportData = dict()
        for root, dirs, files in os.walk(self.source_dir):
            for f in files:
                if f == 'gradle-wrapper.properties':
                    self.check_properties(root, f)
                elif f == 'gradle-wrapper.jar':
                    self.check_jar(root, f)

        if self.report:
            self.reply['report'] = (
                self.get_source_scanning_header('Gradle Wrapper') + self.report
            )

        self.reply['reportData'][self.application_id] = self.reportData
        self.write_json()


if __name__ == '__main__':
    GradleWrapper().main()
