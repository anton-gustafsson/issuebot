#!/usr/bin/env python3
#
# issuebot_apt_install = gradle

import inspect
import json
import os
import re
import sys

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule

PRODUCT_FLAVORS_REGEX = re.compile(r'\s*productFlavors\s*{')


class GradleProductFlavors(IssuebotModule):
    def main(self):
        if not os.path.isdir(self.source_dir):
            print(self.source_dir, 'does not exist, skipping')
            return
        g = self.get_build_gradle_with_android_plugin()
        if not g:
            return
        path = g[0]
        with open(path, errors='surrogateescape') as fp:
            data = fp.read()
            if PRODUCT_FLAVORS_REGEX.search(data):
                self.add_label('productFlavors')
        report = ''

        self.run_gradle_plugin('productFlavors')
        result_json = os.path.join(self.source_dir, 'fdroid-productFlavors.json')
        if os.path.exists(result_json):
            with open(result_json) as fp:
                data = json.load(fp)
            self.reply['reportData']['productFlavors'] = data
            report = ''
            report += '<details><summary><tt>productFlavors</tt></summary><ul>'
            for entry in data:
                report += '<li>{}</li>'.format(entry)
            report += '</ul></details>'

        if report:
            self.reply['report'] = '<h3>Gradle Flavors</h3>' + report
        self.write_json()


if __name__ == "__main__":
    GradleProductFlavors().main()
