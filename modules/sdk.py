#!/usr/bin/env python3

import inspect
import os
import re
import sys

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule


LICENSE_HEADER_REGEX = re.compile(
    r'(license|GNU General Public|GPL|copyright)', flags=re.IGNORECASE
)


class Sdk(IssuebotModule):

    listing = dict()

    def add_to_listing(self, key, root, f):
        self.add_label(key)
        if key not in self.listing:
            self.listing[key] = dict()
        segments = root.split('/')
        path = os.path.join(*segments[2:], f)
        self.listing[key][path] = self.get_source_url(path)

    def main(self):
        if not os.path.isdir(self.source_dir):
            print(self.source_dir, 'does not exist, skipping')
            return
        for root, dirs, files in os.walk(os.path.relpath(self.source_dir)):
            for f in files:
                if f.endswith('.gradle') or f.endswith('.gradle.kts'):
                    self.add_to_listing('gradle', root, f)
                elif f.endswith('.java'):
                    self.add_to_listing('java', root, f)
                elif f.endswith('.kt'):
                    self.add_to_listing('kotlin', root, f)
                elif f in ('build-extras.gradle', 'build.json'):
                    self.add_to_listing('cordova', root, f)
                elif f == 'package.json':
                    with open(os.path.join(root, f), errors='surrogateescape') as fp:
                        contents = fp.read()
                        if 'cordova' in contents:
                            self.add_to_listing('cordova', root, f)
                        if 'react' in contents:
                            self.add_to_listing('react', root, f)
                elif f in ('App.js', 'App.jsx', 'react-native.config.js'):
                    self.add_to_listing('react-native', root, f)
                elif f in ('Android.mk', 'Application.mk'):
                    self.add_to_listing('ndk', root, f)
                elif f == 'build.xml':
                    self.add_to_listing('ant', root, f)
                elif f.lower().startswith('changelog'):
                    self.add_to_listing('changelog', root, f)
                elif f in ('Appfile', 'Fastfile'):
                    self.add_to_listing('fastlane', root, f)
                elif root.endswith('/play') and f in (
                    'contact-email.txt',
                    'contact-phone.txt',
                    'contact-website.txt',
                    'contactEmail',
                    'contactPhone',
                    'contactWebsite',
                    'default-language.txt',
                    'defaultLanguage',
                ):
                    self.add_to_listing('triple-t', root, f)
                elif f in ('.buckconfig', '.buckjavaargs', 'BUCK', '_BUCK'):
                    self.add_to_listing('buck', root, f)
                elif f.endswith('.ninja'):
                    self.add_to_listing('ninja', root, f)
                elif (
                    f.endswith('.cc')
                    or f.endswith('.cpp')
                    or f.endswith('.cxx')
                    or f.endswith('.hh')
                    or f.endswith('.hpp')
                ):
                    self.add_to_listing('c++', root, f)
                elif f.endswith('.c'):
                    self.add_to_listing('c', root, f)
                elif f == 'pubspec.yaml' or f.startswith('.flutter-plugins'):
                    self.add_to_listing('flutter', root, f)
                elif f.endswith('.dart'):
                    self.add_to_listing('dart', root, f)
                elif f == 'google-services.json':
                    self.add_to_listing('google-services', root, f)
                elif f.endswith('.groovy'):
                    self.add_to_listing('groovy', root, f)
                elif f.endswith('.lua'):
                    self.add_to_listing('lua', root, f)
                elif f.endswith('.m'):
                    self.add_to_listing('objc', root, f)
                elif f.endswith('.scala'):
                    self.add_to_listing('scala', root, f)
                elif f.endswith('.cs') or f.endswith('.csproj'):
                    self.add_to_listing('csharp', root, f)
                elif f.endswith('.fs'):
                    self.add_to_listing('fsharp', root, f)
                elif (
                    f.endswith('.sln')
                    or f.endswith('.code-workspace')
                    or f.endswith('.vcproj')
                    or f.endswith('.vcxproj')
                ):
                    self.add_to_listing('visualstudio', root, f)
                elif (
                    f.endswith('.am')
                    or f.endswith('.m4')
                    or f == 'configure'
                    or f == 'configure.in'
                    or f == 'configure.ac'
                ):
                    self.add_to_listing('autotools', root, f)
                elif (
                    f in ('.bazelrc', '.bazelproject', 'BUILD', 'WORKSPACE')
                    or f.endswith('.bzl')
                    or f.endswith('.bazel')
                ):
                    self.add_to_listing('bazel', root, f)
                elif f == 'buildozer.spec' or f.endswith('.kv'):
                    self.add_to_listing('kivy', root, f)
                elif f == '.scons':
                    self.add_to_listing('scons', root, f)
                # Translation
                elif f == 'crowdin.yml':
                    self.add_to_listing('crowdin', root, f)
                elif f == '.weblate':
                    self.add_to_listing('weblate', root, f)
                # CI systems
                elif f == 'appveyor.yml':
                    self.add_to_listing('appveyor', root, f)
                elif f == 'circle.yml':
                    self.add_to_listing('circle-ci', root, f)
                elif f == '.cirrus.yml':
                    self.add_to_listing('cirrus-ci', root, f)
                elif f == '.drone.yml':
                    self.add_to_listing('drone-ci', root, f)
                elif f == '.gitlab-ci.yml':
                    self.add_to_listing('gitlab-ci', root, f)
                elif f == '.travis.yml':
                    self.add_to_listing('travis-ci', root, f)
                # licenses
                elif (
                    f.startswith('LICENSE')
                    or f.startswith('license')
                    or f.startswith('COPYING')
                    or f.startswith('gpl-')
                ):
                    self.add_to_listing('license', root, f)
                elif f.startswith('README'):
                    with open(os.path.join(root, f), errors='surrogateescape') as fp:
                        text = fp.read()
                        m = LICENSE_HEADER_REGEX.search(text)
                        if m:
                            self.add_to_listing('license', root, f)
                elif f == '.gitmodules':
                    self.add_to_listing('git-submodules', root, f)
                elif f.endswith('.xml') and 'res/xml' in root:
                    show = False
                    for word in ('track', 'analy', 'google', 'ga'):
                        if word in f:
                            show = True
                            break
                    if not show:
                        with open(os.path.join(root, f)) as fp:
                            show = 'ga_trackingId' in fp.read()
                    if show:
                        self.add_to_listing('ga_trackingId', root, f)

            for d in dirs:
                if d == '.vscode':
                    self.add_label('vscode')
                elif d == '.tx':
                    self.add_to_listing('transifex', os.path.join(root, d), 'config')
                elif d == '.circleci':
                    self.add_to_listing('circle-ci', root, d)
                elif d == '.cordova':
                    self.add_to_listing('cordova', os.path.join(root, d), 'config.json')
                elif d == 'play':
                    if os.path.isdir(os.path.join(root, d, 'listings')):
                        self.add_to_listing('triple-t', root, d)
                    if os.path.isdir(os.path.join(root, d, 'release-notes')):
                        self.add_to_listing('triple-t', root, d)
                    triplet_dir = os.path.dirname(os.path.dirname(root))
                    if os.path.exists(os.path.join(triplet_dir, 'build.gradle')):
                        self.add_to_listing('triple-t', triplet_dir, 'build.gradle')

        if 'license' not in self.reply['labels']:
            self.add_label('no-license')

        report = self.get_source_scanning_header('SDKs/Languages')
        for sdk, d in sorted(self.listing.items()):
            report += '<details><summary>{}</summary><ul>'.format(sdk)
            for path, url in sorted(d.items()):
                if url:
                    report += '<li><a href="{url}">{path}</a></li>'.format(
                        path=path, url=url
                    )
                else:
                    report += '<li>{}</li>'.format(path)
            report += '</ul></details>'
        self.reply['report'] = report
        self.reply['reportData'] = self.listing
        self.write_json()


if __name__ == "__main__":
    Sdk().main()
